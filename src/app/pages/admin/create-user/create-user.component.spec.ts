import { fakeAsync, async, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { CreateUserComponent } from './create-user.component';
import { By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('CreateUserComponent', () => {
  let component: CreateUserComponent;
  let fixture: ComponentFixture<CreateUserComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        ReactiveFormsModule,
        FormsModule
      ],
      declarations: [ 
        CreateUserComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have firstName field', () => {
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('input[formcontrolname="firstName"]')).toBeTruthy();
    // expect(fixture.nativeElement.querySelector('#firstName')).toBeTruthy();
    // expect(fixture.nativeElement.querySelector('.firstName')).toBeTruthy();
  });

  it('should have lastName field', () => {
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('input[formcontrolname="lastName"]')).toBeTruthy();
  });

  it('should have phoneNumber field', () => {
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('select[formcontrolname="prefix"]')).toBeTruthy();
    expect(fixture.nativeElement.querySelector('input[formcontrolname="phoneNumber"]')).toBeTruthy();
  });

  it('should have roles field', () => {
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('select[formcontrolname="roles"]')).toBeTruthy();
  });

  it('should have roles options', () => {
    fixture.detectChanges();
    const select: HTMLSelectElement = fixture.debugElement.query(By.css('select[formcontrolname="roles"]')).nativeElement;
    expect(select.options.length).toBeTruthy();
  });

  it('should have email field', () => {
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('input[formcontrolname="email"]')).toBeTruthy();
  });

  it('should have create button', () => {
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('button[type="submit"]')).toBeTruthy();
  });

  it('should submit', fakeAsync(() => {
    spyOn(component, 'onSubmit');
    let button = fixture.debugElement.nativeElement.querySelector('button[type="submit"]');
    button.click();
    tick();
    expect(component.onSubmit).toHaveBeenCalled();
  }));
});
